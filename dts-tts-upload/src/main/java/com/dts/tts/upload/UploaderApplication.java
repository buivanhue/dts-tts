package com.dts.tts.upload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
public class UploaderApplication {
    public static void main(String[] args) {
        SpringApplication.run(UploaderApplication.class, args);
    }

    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(5120000000L);
        factory.setMaxRequestSize(5120000000L);
        return factory.createMultipartConfig();

    }

}
