package com.dts.tts.upload.service;

import com.dts.tts.core.base.BaseException;
import com.dts.tts.core.base.Result;
import com.dts.tts.core.exception.ParamInvalidException;
import com.dts.tts.core.tool.FileUtil;
import com.dts.tts.core.tool.FileUtils;
import com.dts.tts.upload.config.UploadConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

@Service
public class UploadService {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public String defaultUpload(String dataBase64) {
        try {
            String[] data = getData(dataBase64);
            String mimeType = getMimeType(FileUtil.extractMimeTypeBase64(data[0]));
            if (!UploadConfig.imgMimeType.contains(mimeType))
                mimeType = UploadConfig.imgMimeDefault;
            String fileName = FileUtils.generateFileName(UploadConfig.imgPrefix) + "." + mimeType;
            String relativeFolder = getRelativePath();
            if (!FileUtils.checkFolder(UploadConfig.path + File.separator + relativeFolder))
                FileUtils.createOutFolder(UploadConfig.path + File.separator + relativeFolder);

            FileUtil.writeByteToImageFile(data[1], UploadConfig.path + File.separator + relativeFolder + File.separator + fileName);
            return UploadConfig.shareUrl + relativeFolder + File.separator + fileName;
        } catch (Exception e) {
            logger.error("Exception_{}", e);
        }

        throw new BaseException();
    }

    private String getMimeType(String mime) {
        if (StringUtils.hasLength(mime) && mime.contains("/"))
            return mime.split("/")[1];
        throw new ParamInvalidException("Định dạng không hỗi trợ");
    }

    private String[] getData(String base64) {
        if (!StringUtils.hasLength(base64))
            throw new ParamInvalidException("Data không được để trống.");

        String data[] = base64.split(",");
        if (data.length < 2)
            throw new ParamInvalidException("Data không hợp lệ.");
        return data;
    }

    public String getRelativePath() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        String relativeFolderPath = cal.get(Calendar.YEAR) + File.separator + (cal.get(Calendar.MONTH) + 1) + File.separator + cal.get(Calendar.DAY_OF_MONTH);
        return relativeFolderPath;
    }

    public static String generateFileName() {
        return UUID.randomUUID().toString();
    }

    public Result uploadWithBlock(File file) {
        Result result = new Result();
        try {
            long maxParts = getMaximumNumberOfParts(file.length());

            String md5 = generateFileName();
            logger.info("md5_" + md5);
            logger.info("NameFile_" + file.getName());
            logger.info("maxParts" + maxParts);

            int segment = 0;
            byte[] buffer = new byte[1024 * 1024];
            int bytesRead;
            BufferedInputStream inStream = null;
            try {
                inStream = new BufferedInputStream(new FileInputStream(file));
                do {
                    bytesRead = inStream.read(buffer, 0, buffer.length);
                    if (bytesRead == -1) {
                        continue;
                    }

                    byte[] finalBuffer;
                    if (buffer.length > bytesRead) {
                        finalBuffer = Arrays.copyOf(buffer, bytesRead);
                    } else {
                        finalBuffer = buffer;
                    }

                    String s64 = Base64.getEncoder().encodeToString(finalBuffer);

                    result = transferUploadToServerOtherBlock(file.getName(), file.getName(), md5, s64, segment, maxParts, finalBuffer.length, file.length());

                    segment++;

                    logger.info("result_" + result);

                } while (bytesRead != -1);
            } finally {
                if (inStream != null) {
                    inStream.close();
                }
            }

            return result;
        } catch (Exception e) {
            logger.info("Exception_" + e);
            return Result.fail("");
        }
    }

    public Result transferUploadToServerOtherBlock(String nameFile, String mimeType, String md5, String bytes, int chunk, long chunks, long srcSize, long targetSize) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
            map.add("nameFile", nameFile);
            map.add("mimeType", mimeType);
            map.add("bytes", bytes);
            map.add("chunk", chunk);
            map.add("chunks", chunks);
            map.add("srcSize", srcSize);
            map.add("targetSize", targetSize);
            map.add("md5", md5);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
            Result response = restTemplate.postForObject("http://localhost:7010/api/v1/upload/file-block", requestEntity, Result.class);

            return response;
        } catch (Exception e) {
            logger.info("Exception_{}", e);
        }
        return null;
    }

    private static final DecimalFormat df = new DecimalFormat("0");

    public static long getMaximumNumberOfParts(long size) {
        Double numberOfParts = (double) size / (1024 * 1024);
        if (numberOfParts == 0) {
            return 1;
        }

        df.setRoundingMode(RoundingMode.UP);
        return Long.parseLong(df.format(numberOfParts));
    }


}
