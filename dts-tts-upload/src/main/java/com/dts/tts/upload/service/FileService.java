package com.dts.tts.upload.service;

import com.dts.tts.core.enums.ResultEnum;
import com.dts.tts.core.tool.FileUtils;
import com.dts.tts.upload.config.UploadConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import static com.dts.tts.upload.utils.UploadUtils.*;

@Service
public class FileService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public Integer uploadWithBlock2(String path,
                                    String md5,
                                    Long targetSize,
                                    Long srcSize,
                                    Integer chunks,
                                    Integer chunk,
                                    byte[] bytes) {
        try {
            InputStream myInputStream = new ByteArrayInputStream(bytes);

            getFileName(md5, chunks);

            FileUtils.writeWithBlok(path, targetSize, myInputStream, srcSize, chunks, chunk);
            addChunk(md5, chunk);
            if (isUploaded(md5)) {
                removeKey(md5);

                return ResultEnum.SUCCESS_FILE.getCode();
            }
            return chunk;
        } catch (Exception e) {
            logger.error("Exception {}", e);
            return -1;
        }
    }

}
