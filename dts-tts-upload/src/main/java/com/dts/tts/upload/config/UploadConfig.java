package com.dts.tts.upload.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;


@Configuration
public class UploadConfig {
    public final static List<String> imgMimeType = Arrays.asList("png", "jpg", "jpeg", "gif", "tiff", "bmp");

    public static String videoPrefix = "dts-tts-video";
    public static String imgPrefix = "dts-tts-image";
    public static String path;
    public static String shareUrl;
    public static String imgMimeDefault = "png";

    @Value("${upload.path}")
    public void setPath(String path) {
        UploadConfig.path = path;
    }

    @Value("${upload.shareUrl}")
    public void setShareUrl(String shareUrl) {
        UploadConfig.shareUrl = shareUrl;
    }

}
