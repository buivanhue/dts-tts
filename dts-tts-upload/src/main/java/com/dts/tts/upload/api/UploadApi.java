package com.dts.tts.upload.api;

import com.dts.tts.core.base.Base;
import com.dts.tts.core.base.BaseException;
import com.dts.tts.core.base.Result;
import com.dts.tts.core.enums.ResultEnum;
import com.dts.tts.core.tool.FileUtil;
import com.dts.tts.core.tool.FileUtils;
import com.dts.tts.core.tool.StringUtl;
import com.dts.tts.upload.config.UploadConfig;
import com.dts.tts.upload.request.UploadImgRequest;
import com.dts.tts.upload.service.FileService;
import com.dts.tts.upload.service.UploadService;
import com.github.kiulian.downloader.YoutubeDownloader;
import com.github.kiulian.downloader.model.YoutubeVideo;
import com.github.kiulian.downloader.model.formats.AudioVideoFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.*;

@RestController
@RequestMapping("/api/v1/upload")
public class UploadApi extends Base {

    @Autowired
    private FileService fileService;

    @Autowired
    private UploadService uploadService;

    @PostMapping("/imageBase64/default")
    public Result defaultBase64Upload(@RequestBody UploadImgRequest request) {
        return Result.success(uploadService.defaultUpload(request.getData()));
    }

    @PostMapping("/file")
    @CrossOrigin(origins = "*")
    public Result uploadFile(@RequestParam("file") MultipartFile file) {
        try {
            String mimeType = file.getContentType();
            logger.info("mimeType=>>{}", mimeType);
            logger.info("fileName=>>{}", file.getOriginalFilename());
            String type = mimeType.split("/")[1];
            if (type.equals("*"))
                type = "mp4";
            if (!mimeType.contains("video")) {
                return Result.error("Lỗi định dạng video");
            }

            String fileName = FileUtils.generateFileName(UploadConfig.videoPrefix);
            logger.info("fileName =>>{}", fileName);

            String relativeFolderPath = getRelativePath();
            String relativePath = relativeFolderPath + File.separator + fileName + "." + type;
            if (FileUtils.checkFolder(UploadConfig.path + File.separator + relativeFolderPath) == false)
                FileUtils.createOutFolder(UploadConfig.path + File.separator + relativeFolderPath);

            File newFile = new File(UploadConfig.path + File.separator + relativePath);
            OutputStream outStream = new FileOutputStream(newFile);
            InputStream in = file.getInputStream();
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1)
                outStream.write(buffer, 0, bytesRead);

            outStream.close();
            in.close();

            return Result.success();

        } catch (Exception e) {
            logger.error("Exception {}", e);
            throw new BaseException();
        }
    }

    @GetMapping("/fileYoutube")
    public Result getFileYoutube(@RequestParam("link") String linkYoutube) {

        logger.info("LinkYoutube {}", linkYoutube);

        if (!linkYoutube.contains("https://www.youtube.com/watch?v="))
            throw new BaseException("Link không hợp lệ");

        String videoId = linkYoutube.replace("https://www.youtube.com/watch?v=", "");

        YoutubeDownloader downloader = new YoutubeDownloader();
        downloader.addCipherFunctionPattern(2, "\\b([a-zA-Z0-9$]{2})\\s*=\\s*function\\(\\s*a\\s*\\)\\s*\\{\\s*a\\s*=\\s*a\\.split\\(\\s*\"\"\\s*\\)");
        downloader.setParserRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
        downloader.setParserRetryOnFailure(1);
        YoutubeVideo video;
        try {
            video = downloader.getVideo(videoId);

            // filtering only video formats
            List<AudioVideoFormat> videoWithAudioFormats = video.videoWithAudioFormats();
            if (videoWithAudioFormats.size() < 0) {
                logger.info("Lỗi ở lấy videoWithAudioFormats {}", videoWithAudioFormats);
                throw new BaseException("Quá trình download video xảy ra lỗi");
            }
            List<AudioVideoFormat> videoFormats = new ArrayList<>();
            for (int item = 0; item < videoWithAudioFormats.size(); item++) {
                if (videoWithAudioFormats.get(item).qualityLabel().equals("720p")) {
                    videoFormats.add(videoWithAudioFormats.get(item));
                    break;
                }
            }

            if (videoFormats.size() == 0) {
                videoFormats.add(videoWithAudioFormats.get(0));
            }

            if (videoFormats.size() == 0) {
                throw new BaseException("Chất lượng video < 360p");
            }

            File outputDir = new File("D:\\");

            // sync downloading
            File file = video.download(videoFormats.get(0), outputDir);
            if (!file.exists())
                throw new BaseException("Quá trình download video xảy ra lỗi");

            String fileName = FileUtils.generateFileName(UploadConfig.videoPrefix);
            File fileRename = new File(file.getPath().replace(file.getName(), fileName + "." + "mp4"));
            boolean renameFle = file.renameTo(fileRename);
            logger.info("renameFle {}", renameFle);


            Map<String, Object> map = uploadFileYoutube(fileRename);
            if (map == null) {
                throw new BaseException("Quá trình download video xảy ra lỗi");
            }

            map.put("title", video.details().title());
            map.put("description", video.details().description());
            List<String> hastags = new ArrayList<>();
            if (!video.details().keywords().isEmpty()) {
                video.details().keywords().stream().forEach(item -> {
                    hastags.add(StringUtl.getAnsiString(item).replaceAll("( )+", ""));
                });
            }
            map.put("hastags", hastags);

            try {
                logger.info("Delete file download from youtube {}", Files.deleteIfExists(file.toPath()));
            } catch (Exception e) {
                logger.info("Delete file download from youtube exception {}", e);
            }

            return Result.success(map);
        } catch (Exception e) {
            logger.error("Error download file from youtube {}", e);
            throw new BaseException("Quá trình download video xảy ra lỗi");
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/file-block")
    public Result uploadFileBlock(@RequestParam(value = "mimeType") String mimeType,
                                  @RequestParam(value = "nameFile") String nameFile,
                                  @RequestParam(value = "md5") String md5,
                                  @RequestParam(value = "targetSize") Long targetSize,
                                  @RequestParam(value = "srcSize") Long srcSize,
                                  @RequestParam(value = "chunks") Integer chunks,
                                  @RequestParam(value = "chunk") Integer chunk,
                                  @RequestParam(value = "bytes") String byteString) {
        if (chunk == null || chunks == null) {
            return Result.fail("chunk || chunks không được để trống.");
        }

        if (!StringUtils.hasLength(byteString)) {
            return Result.fail("bytes không được để trống.");
        }

        byte[] bytes = Base64.getDecoder().decode(byteString);
        try {
            logger.info("mimeType=>>{}", mimeType);
            logger.info("fileName=>>{}", nameFile);
            logger.info("md5=>>{}", md5);
            logger.info("chunk=>>{}", chunk);
            logger.info("chunks=>>{}", chunks);

            String fileName = UploadConfig.videoPrefix + "-" + md5 + "." + mimeType;
            logger.info("fileName =>>{}", fileName);

            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            String relativeFolderPath = UploadConfig.path + File.separator + cal.get(Calendar.YEAR) +
                    File.separator + (cal.get(Calendar.MONTH) + 1) +
                    File.separator + cal.get(Calendar.DAY_OF_MONTH);

            String relativePath = relativeFolderPath + File.separator + fileName;
            if (FileUtils.checkFolder(relativeFolderPath) == false)
                FileUtils.createOutFolder(relativeFolderPath);

            int returnUploadWithBlock = fileService.uploadWithBlock2(relativePath, md5, targetSize, srcSize, chunks, chunk, bytes);
            if (returnUploadWithBlock == -1) {
                int deleteFile = FileUtil.deleteFile(new File(relativePath));
                logger.info("deleteFile_" + fileName);
                throw new BaseException();
            } else if (returnUploadWithBlock == ResultEnum.SUCCESS_FILE.getCode()) {
                File newFile = new File(relativePath);
                if (!newFile.exists()) {
                    throw new BaseException();
                }

                Map<String, Object> re = new HashMap<>();

                return Result.success(re);

            } else {
                Result result = new Result();
                Map<String, Object> map = new HashMap<>();
                map.put("chunk", returnUploadWithBlock);
                map.put("md5", md5);
                map.put("nameFile", fileName);

                result.setData(map);
                result.setMsg(ResultEnum.SUCCESS_CHUNK_FILE.getDesc());
                result.setCode(ResultEnum.SUCCESS_CHUNK_FILE.getCode());
                return result;
            }
        } catch (Exception e) {
            logger.error("err {}", e);
            throw new BaseException();
        }
    }

    @PostMapping("/client-file-block")
    public Result uploadClientFileBlock() {
        File file = new File("H:\\video\\livestream\\SPIDERMAN2.mp4");
        return uploadService.uploadWithBlock(file);
    }

    public String getRelativePath() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        String relativeFolderPath = cal.get(Calendar.YEAR) + File.separator + (cal.get(Calendar.MONTH) + 1) + File.separator + cal.get(Calendar.DAY_OF_MONTH);
        return relativeFolderPath;
    }

    public Map<String, Object> uploadFileGeneral(File newFile) {
        try {
            String fileName = FileUtils.generateFileName(UploadConfig.videoPrefix);
            logger.info("fileName =>>{}", fileName);

            logger.info("newFile =>>{}", newFile.getAbsoluteFile());

            String relativeFolderPath = uploadService.getRelativePath();
            if (!FileUtils.checkFolder(UploadConfig.path + File.separator + relativeFolderPath))
                FileUtils.createOutFolder(UploadConfig.path + File.separator + relativeFolderPath);

        } catch (Exception e) {
            try {
                logger.info("Delete file download from youtube {}", Files.deleteIfExists(newFile.toPath()));
            } catch (Exception ex) {
                logger.error("err", e);
                throw new BaseException("Upload error");
            }
        }
        return null;
    }

    public Map<String, Object> uploadFileYoutube(File fileNew) {
        return uploadFileGeneral(fileNew);
    }

}
