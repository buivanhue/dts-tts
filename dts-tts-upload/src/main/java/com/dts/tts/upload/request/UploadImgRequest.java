package com.dts.tts.upload.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UploadImgRequest{
    private String data;
    private Integer width;
    private Integer height;

}
