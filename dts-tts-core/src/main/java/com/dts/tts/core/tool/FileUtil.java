package com.dts.tts.core.tool;

import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.util.StringUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FileUtil {
    public static void writeByteToImageFile(String dataImage, String fileNameImage) throws Exception {
        byte[] bImg64 = dataImage.getBytes();
        byte[] bImg = Base64.decodeBase64(bImg64);
        try (FileOutputStream fos = new FileOutputStream(fileNameImage)) {
            fos.write(bImg);
        } catch (IOException ex) {
            throw new Exception("Write file to " + fileNameImage + " failed !!!");
        }
    }

    //data:image/jpeg;base64,/
    public static String extractMimeTypeBase64(final String encoded) {
        final Pattern mime = Pattern.compile("^data:([a-zA-Z0-9]+/[a-zA-Z0-9]+).*");
        final Matcher matcher = mime.matcher(encoded);
        if (!matcher.find())
            return "";
        return matcher.group(1).toLowerCase();
    }


    public static int deleteFile(String filePath) {
        try {
            File file = new File(filePath);
            return deleteFile(file);
        } catch (Exception e) {
        }
        return -1;
    }

    public static int deleteFile(File file) {
        try {
            if (file != null) {
                if (file.delete()) {
                    //xóa thành công
                    return 0;
                } else {
                    return 1;
                }
            }
        } catch (Exception e) {
        }
        return -1;
    }


    public static boolean resize(BufferedImage source, File dist, int width, int height, float quality, String format) throws IOException {
        Thumbnails.Builder<BufferedImage> thumbBuilder = Thumbnails.of(source);
        if (width == -1 || height == -1) {
            if (width != -1) {
                thumbBuilder.width(width);
            } else if (height != -1) {
                thumbBuilder.height(height);
            }
        } else {
            thumbBuilder.size(width, height).keepAspectRatio(false);
        }
        if (quality > 0.0f) {
            thumbBuilder.outputQuality(quality);
        }
        thumbBuilder.outputFormat(format).toFile(dist);
        return true;
    }

    public static void append(String filePath, String line) throws IOException {
        File file = new File(filePath);
        FileUtils.writeStringToFile(
                file, line, StandardCharsets.UTF_8, true);
    }

    public static String getFileNameSuccess(Date time, int packageId) {
        return String.format("Success_%d_%s.txt", packageId, DateUtil.getStringPartTime(time));
    }

    public static String getFileNameError(Date time, int packageId) {
        return String.format("Error_%d_%s.txt", packageId, DateUtil.getStringPartTime(time));
    }
    
    public static String getFileNameError(String time, int packageId) {
        return String.format("Error_%d_%s.txt", packageId, time);
    }

    public static String getFileNameErrorPackNotFound(Date time, int packageId) {
        return String.format("Error_PackNotFound_%d_%s.txt", packageId, DateUtil.getStringPartTime(time));
    }

    public static String getFileNameErrorUserNotFound(Date time, int packageId) {
        return String.format("Error_UserNotFound_%d_%s.txt", packageId, DateUtil.getStringPartTime(time));
    }

    public static String getTrancodeFileNameError(Date time, String infoId) {
        return String.format("Trancode_Error_%s_%s.txt", infoId, DateUtil.getStringPartTime(time));
    }

    public static String getFileNameRenewalError(Date time, int packageId) {
        return String.format("RenewalError_%d_%s.txt", packageId, DateUtil.getStringPartTime(time));
    }


    public static String getImageFileName(String fileName) {
        String ex = FilenameUtils.getExtension(fileName);
        if (StringUtils.hasLength(ex))
            fileName = fileName.replace("." + ex, "");
        fileName = fileName.replace(" ", "-");
        return "meclip-image-" + OtpUtil.generate(10) + "-" + new Date().getTime() + "-" + fileName;
    }

    public static String getVideoFileName(String fileName) {
        String ex = FilenameUtils.getExtension(fileName);
        if (StringUtils.hasLength(ex))
            fileName = fileName.replace("." + ex, "");
        fileName = fileName.replace(" ", "-");
        return "meclip-video-" + OtpUtil.generate(8) + "-" + new Date().getTime() + "-" + fileName;
    }

    public static String getFileName(String fileName) {
        String ex = FilenameUtils.getExtension(fileName);
        if (StringUtils.hasLength(ex))
            fileName = fileName.replace("." + ex, "");
        fileName = fileName.replace(" ", "-");
        return OtpUtil.generate(8) + "-" + new Date().getTime() + "-" + fileName;
    }


}
