package com.dts.tts.core.exception;

import com.dts.tts.core.base.BaseException;
import com.dts.tts.core.enums.ResultEnum;

public class ParamInvalidException extends BaseException {
    public ParamInvalidException() {
        super(ResultEnum.PARAM_INVALID_ERROR.getDesc());
        this.code = ResultEnum.PARAM_INVALID_ERROR.getCode();
    }

    public ParamInvalidException(String msg) {
        super(msg);
        this.code = ResultEnum.PARAM_INVALID_ERROR.getCode();
    }

}
