package com.dts.tts.core.base;

import com.dts.tts.core.enums.ResultEnum;
import lombok.Getter;


@Getter
public class BaseException extends RuntimeException {

    private static final long serialVersionUID = -2831588602667996487L;

    protected int code;


    public BaseException() {
        super("SYSTEM_ERROR");
        this.code = 99;
    }

    public BaseException(ResultEnum resultEnum) {
        super(resultEnum.getDesc());
        this.code = resultEnum.getCode();
    }

    public BaseException(String message) {
        super(message);
        this.code = 99;
    }

    public BaseException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}

