package com.dts.tts.core.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Base {
    protected static final String REGEX_MOBILE = "^((13[0-9])|(14[5,7,9])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199)\\d{8}$";// 手机号码校验

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static int corePoolSize = Runtime.getRuntime().availableProcessors();

    //fix tam chi co 4 tien trinh xu ly transcode thoi
    protected static final ExecutorService callbackExecutor = Executors.newFixedThreadPool(4);

    public final static String USER_X_ID = "USER-X-ID";
    public final static String X_WAP_MSISDN = "X-Wap-MSISDN";


    protected static String getString(Map<String, Object> map, String key) {
        if (null != map.get(key)) {
            return map.get(key).toString();
        }
        return "";
    }

    protected static Map<String, Object> getMap() {
        return new HashMap<>();
    }

    protected int getPage(int start, int limit) {
        if (start <= 0)
            return 0;
        return start / limit;
    }

}
